import numpy as np

from sklearn import preprocessing
from sklearn.model_selection import GroupShuffleSplit
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.metrics import accuracy_score
from sklearn import svm
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier

# Variables
n_splits = 10
test_size = 0.2

# 1. --Loading data. --
X_train = np.load('./data/X_train_kaggle.npy')
X_test = np.load('./data/X_test_kaggle.npy')
y_train = np.loadtxt('./data/y_train_final_kaggle.csv', dtype=str, delimiter=',', usecols=(0, 1), unpack=True)
groups = np.loadtxt('./data/groups.csv', dtype=str, delimiter=',', usecols=(0, 1, 2), unpack=True)


# 2. -- Create an index of class names --
le = preprocessing.LabelEncoder()
le.fit(y_train[1])
y_train_le = le.transform(y_train[1])


# 3. -- Splitting data into train and test --
train_indexes = []
test_indexes = []
X_subtrain = []
y_subtrain = []
X_subtest = []
y_subtest = []

gss = GroupShuffleSplit(n_splits, test_size=test_size, random_state=0)
repr(gss)

# Spliting train and test values fo the n splittings, having n arrays of index of each train and test samples
for train, test in gss.split(y_train_le, groups=groups[1]):
    train_indexes.append(train)
    test_indexes.append(test)

for i, indexes in enumerate(train_indexes):
    aux1 = [X_train[i] for i in indexes]
    aux1 = np.array(aux1)
    X_subtrain.append(aux1)
    aux2 = [y_train_le[i] for i in indexes]
    aux2 = np.array(aux2)
    y_subtrain.append(aux2)

for i, indexes in enumerate(test_indexes):
    aux1 = [X_train[i] for i in indexes]
    aux1 = np.array(aux1)
    X_subtest.append(aux1)
    aux2 = [y_train_le[i] for i in indexes]
    aux2 = np.array(aux2)
    y_subtest.append(aux2)


# 4. -- Extract features --
# Mean and std extraction
def extract_mean_std(data, splits):
    features = []
    for j in range(splits):
        means = np.mean(data[j], axis=2, dtype=np.float64)
        features1 = np.concatenate((means, np.std(data[j], axis=2, dtype=np.float64)), axis=1)
        features.append(features1)
    return features


features_test = extract_mean_std(X_subtest, n_splits)
features_train = extract_mean_std(X_subtrain, n_splits)


# 5. -- Applying multiple classifiers analysis --
classifiers = [LinearDiscriminantAnalysis(), svm.SVC(gamma='auto', kernel='linear'),
               svm.SVC(gamma='auto', kernel='poly'), LogisticRegression(multi_class='auto', solver='lbfgs', max_iter=10000),
               RandomForestClassifier(n_estimators=100)]
class_names = ['LDA', 'SVM Linear', 'SVM Polynomial', 'Logistic Regresion', 'Random Forest']
scores = []

for classifier in classifiers:
    aux = []
    for split in range(n_splits):
        classifier.fit(features_train[split], y_subtrain[split])
        score = accuracy_score(classifier.predict(features_test[split]), y_subtest[split])
        aux.append(score)
    scores.append(aux)

# Getting an array with the mean of the scores of each classifier.
score_means = np.mean(np.array(scores), axis=1)
for index, score in enumerate(score_means):
    print(class_names[index] + 'score: ' + str(score))


# 6. -- Create submission file --
# Getting the classifier with the most accuracy in our own testing.
best_cls = classifiers[int(np.argmax(score_means))]
print("Best classifier: " + str(best_cls))

# Calculating the input features similar to the previous parts.
means_train = np.mean(X_train, axis=2, dtype=np.float64)
X_train = np.concatenate((means_train, np.std(X_train, axis=2, dtype=np.float64)), axis=1)

means_test = np.mean(X_test, axis=2, dtype=np.float64)
X_test = np.concatenate((means_test, np.std(X_test, axis=2, dtype=np.float64)), axis=1)

# Fitting the best classifier in this case
best_cls.fit(X_train, y_train_le)

# Getting the labels of the predictions by the model
y_pred = best_cls.predict(X_test)
# Revert the label encoder into de original classes
labels = list(le.inverse_transform(y_pred))
with open("submission.csv", "w") as fp:
    fp.write("# Id,Surface\n")
    for i, label in enumerate(labels):
        fp.write("%d,%s\n" % (i, label))


